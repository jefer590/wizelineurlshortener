﻿using System;

namespace WizelineUrlShortener.Extensions
{
    public static class DateTimeExtension
    {
        public static long ToEpochTimeTicks(this DateTime dateTime)
        {
            return (dateTime - new DateTime(1970, 1, 1)).Ticks;
        }
    }
}