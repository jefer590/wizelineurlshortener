﻿using System.Data;
using Dapper.FastCrud;
using MySql.Data.MySqlClient;
using WizelineUrlShortener.DataAccess.Infrastructure.Interfaces;

namespace WizelineUrlShortener.DataAccess.Infrastructure
{
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly string _connectionString;

        public ConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbConnection Connection
        {
            get
            {
                var conn = new MySqlConnection(_connectionString);
                OrmConfiguration.DefaultDialect = SqlDialect.MySql;
                conn.Open();
                return conn;
            }
        }

        public void Dispose()
        {
            Connection?.Close();
            Connection?.Dispose();
        }
    }
}