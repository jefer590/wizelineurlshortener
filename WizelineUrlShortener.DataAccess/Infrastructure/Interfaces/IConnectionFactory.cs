﻿using System;
using System.Data;

namespace WizelineUrlShortener.DataAccess.Infrastructure.Interfaces
{
    public interface IConnectionFactory : IDisposable
    {
        IDbConnection Connection { get; }
    }
}