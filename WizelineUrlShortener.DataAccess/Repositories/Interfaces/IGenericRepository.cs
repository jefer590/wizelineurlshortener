﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WizelineUrlShortener.Data.Model;

namespace WizelineUrlShortener.DataAccess.Repositories.Interfaces
{
    public interface IGenericRepository<TEntity, TPK> : IDisposable where TEntity : class, IEntity<TPK>, new()
    {
        Task AddAsync(TEntity entity);
        Task<TEntity> GetAsync(TPK Id);
        Task DeleteAsync(TPK id);
        Task UpdateAsync(TEntity entity);
        void SaveChanges();
    }
}