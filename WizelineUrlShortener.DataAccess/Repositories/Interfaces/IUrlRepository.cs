﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WizelineUrlShortener.Data.Dtos;
using WizelineUrlShortener.Data.Model;

namespace WizelineUrlShortener.DataAccess.Repositories.Interfaces
{
    public interface IUrlRepository : IGenericRepository<UrlShort, ulong>
    {
        Task<UrlShort> GetByShortUrlKey(string shortUrlKey);

        Task<UrlShort> GetByUrlAsync(string url);

        Task<UrlShort> GetByUrlOrShortKey(string url, string shortKey);

        Task<IList<UrlShort>> GetListByUrl(string url);

        Task<ulong> GetNextId();

        Task<ICollection<ListShortUrlDto>> GetPageItems(TablePageParamsDto pageParams);

        Task<long> GetCount();

        Task<bool> IsShortUrlTaken(string modelCustomKey);
    }
}