﻿using WizelineUrlShortener.Data.Model;

namespace WizelineUrlShortener.DataAccess.Repositories.Interfaces
{
    public interface IDapperRepository : IGenericRepository<Entity, long>
    {
    }
}