﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.FastCrud;
using WizelineUrlShortener.Data.Dtos;
using WizelineUrlShortener.Data.Model;
using WizelineUrlShortener.DataAccess.Infrastructure.Interfaces;
using WizelineUrlShortener.DataAccess.Repositories.Interfaces;

namespace WizelineUrlShortener.DataAccess.Repositories
{
    public class UrlRepository : GenericRepository<UrlShort, ulong>, IUrlRepository
    {
        private readonly IConnectionFactory _connectionFactory;

        private IDbTransaction _transaction;

        public UrlRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public override void Dispose()
        {
            _transaction?.Dispose();
            _connectionFactory?.Dispose();
        }

        public override async Task AddAsync(UrlShort entity)
        {
            BeginTransaction();
            await _connectionFactory.Connection.InsertAsync(entity, stmnt => stmnt.AttachToTransaction(_transaction));
        }

        public override Task<UrlShort> GetAsync(ulong id)
        {
            return _connectionFactory.Connection.GetAsync(new UrlShort {Id = id});
        }

        public override async Task DeleteAsync(ulong id)
        {
            BeginTransaction();
            await _connectionFactory.Connection.DeleteAsync(new UrlShort {Id = id},
                stmnt => stmnt.AttachToTransaction(_transaction));
        }

        public override async Task UpdateAsync(UrlShort entity)
        {
            BeginTransaction();
            await _connectionFactory.Connection.UpdateAsync(entity, stmnt => stmnt.AttachToTransaction(_transaction));
        }

        public override void SaveChanges()
        {
            if (_transaction == null) return;

            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
            }
        }

        public async Task<UrlShort> GetByShortUrlKey(string shortUrlKey)
        {
            var resultList = await _connectionFactory.Connection.FindAsync<UrlShort>(stmnt =>
                stmnt.Where($"{nameof(UrlShort.ShortUrlKey):C}=@ShortUrlKey")
                    .Top(1)
                    .WithParameters(new {ShortUrlKey = shortUrlKey}));

            return resultList.FirstOrDefault();
        }

        public async Task<UrlShort> GetByUrlAsync(string url)
        {
            var resultList = await _connectionFactory.Connection.FindAsync<UrlShort>(stmnt =>
                stmnt.Where($"{nameof(UrlShort.Url):C}=@Url")
                    .WithParameters(new {Url = url}));

            return resultList.FirstOrDefault();
        }

        public async Task<ulong> GetNextId()
        {
            const string queryNextAutoIncrement =
                "SELECT Auto_increment FROM information_schema.tables WHERE table_name=@TableName;";
            var result = await _connectionFactory.Connection.QueryAsync<dynamic>(queryNextAutoIncrement,
                new {TableName = $"{nameof(UrlShort).ToLower()}s"});
            return result.First().Auto_increment;
        }

        public async Task<ICollection<ListShortUrlDto>> GetPageItems(TablePageParamsDto pageParams)
        {
            const string query = "SELECT url AS longurl, shorturlkey AS shorturl " +
                                 "FROM urlshorts " +
                                 "LIMIT @Offset,@ItemsPerPage";
            var result = await _connectionFactory.Connection.QueryAsync<ListShortUrlDto>(query,
                new
                {
                    Offset = (pageParams.PageNumber - 1) * pageParams.ItemsPerPage,
                    ItemsPerPage = pageParams.ItemsPerPage
                });

            return result.ToList();
        }

        public async Task<long> GetCount()
        {
            return await _connectionFactory.Connection.CountAsync<UrlShort>();
        }

        public async Task<UrlShort> GetByUrlOrShortKey(string url, string shortKey)
        {
            const string sqlQuery = "SELECT * " +
                                    "FROM urlshorts " +
                                    "WHERE url = @Url OR shorturlkey = @ShortKey";
            var result =
                await _connectionFactory.Connection.QueryAsync<UrlShort>(sqlQuery,
                    new
                    {
                        Url = url,
                        ShortKey = shortKey
                    });

            return result.FirstOrDefault();
        }

        private void BeginTransaction()
        {
            if (_transaction == null)
                _transaction = _connectionFactory.Connection.BeginTransaction();
        }

        public async Task<bool> IsShortUrlTaken(string modelCustomKey)
        {
            var sqlQuery = "select count(1) from urlshorts where shorturlkey = @UrlKey";
            var result = await _connectionFactory.Connection.ExecuteScalarAsync<bool>(sqlQuery,
                new
                {
                    UrlKey = modelCustomKey
                });

            return result;
        }

        public async Task<IList<UrlShort>> GetListByUrl(string url)
        {
            var resultList = await _connectionFactory.Connection.FindAsync<UrlShort>(stmnt =>
                            stmnt.Where($"{nameof(UrlShort.Url):C}=@Url")
                                .WithParameters(new { Url = url }));

            return resultList.ToList();
        }
    }
}