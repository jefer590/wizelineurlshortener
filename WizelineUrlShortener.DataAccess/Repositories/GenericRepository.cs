﻿using System.Threading.Tasks;
using WizelineUrlShortener.Data.Model;
using WizelineUrlShortener.DataAccess.Repositories.Interfaces;

namespace WizelineUrlShortener.DataAccess.Repositories
{
    public abstract class GenericRepository<TEntity, TPK> : IGenericRepository<TEntity, TPK> where TEntity : class, IEntity<TPK>, new()
    {
        public abstract void Dispose();

        public abstract Task AddAsync(TEntity entity);

        public abstract Task<TEntity> GetAsync(TPK Id);

        public abstract Task DeleteAsync(TPK id);

        public abstract Task UpdateAsync(TEntity entity);

        public abstract void SaveChanges();
    }
}