﻿using System.Data;
using System.Threading.Tasks;
using Dapper.FastCrud;
using WizelineUrlShortener.Data.Model;
using WizelineUrlShortener.DataAccess.Infrastructure.Interfaces;
using WizelineUrlShortener.DataAccess.Repositories.Interfaces;

namespace WizelineUrlShortener.DataAccess.Repositories
{
    public class DapperRepository : GenericRepository<Entity, long>, IDapperRepository
    {
        protected readonly IConnectionFactory _connectionFactory;

        private IDbTransaction _transaction;

        public DapperRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public override void Dispose()
        {
            _transaction?.Dispose();
            _connectionFactory?.Dispose();
        }

        public override async Task AddAsync(Entity entity)
        {
            BeginTransaction();
           await _connectionFactory.Connection.InsertAsync(entity, stmnt => stmnt.AttachToTransaction(_transaction));
        }

        public override Task<Entity> GetAsync(long id)
        {
            return _connectionFactory.Connection.GetAsync(new Entity { Id = id });
        }

        public override async Task DeleteAsync(long id)
        {
            BeginTransaction();
            await _connectionFactory.Connection.DeleteAsync(new Entity { Id = id }, stmnt => stmnt.AttachToTransaction(_transaction));
        }

        public override async Task UpdateAsync(Entity entity)
        {
            BeginTransaction();
            await _connectionFactory.Connection.UpdateAsync(entity, stmnt => stmnt.AttachToTransaction(_transaction));
        }

        private void BeginTransaction()
        {
            if(_transaction == null)
                _transaction = _connectionFactory.Connection.BeginTransaction();
        }

        public override void SaveChanges()
        {
            if (_transaction == null) return;

            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
            }
        }
    }
}