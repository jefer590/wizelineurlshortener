﻿using System;
using System.Configuration;
using Autofac;
using WizelineUrlShortener.DataAccess.Infrastructure;
using WizelineUrlShortener.DataAccess.Infrastructure.Interfaces;
using WizelineUrlShortener.DataAccess.Repositories;
using WizelineUrlShortener.DataAccess.Repositories.Interfaces;

namespace WizelineUrlShortener.DataAccess
{
    public class DataAccessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["MariaDbConnection"]?.ConnectionString;

            if(string.IsNullOrEmpty(connectionString))
                throw new Exception("Please provided a connection string for \"MariaDbConnection\"");

            builder.Register(x => new ConnectionFactory(connectionString))
                .As<IConnectionFactory>()
                .InstancePerLifetimeScope();

            // TODO: Register repositories
            builder.Register(x => new UrlRepository(x.Resolve<IConnectionFactory>()))
                .As<IUrlRepository>()
                .InstancePerLifetimeScope();
        }
    }
}