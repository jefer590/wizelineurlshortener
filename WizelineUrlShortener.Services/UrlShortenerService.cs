﻿using System;
using WizelineUrlShortener.Data.Dtos;
using WizelineUrlShortener.Data.Model;
using WizelineUrlShortener.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WizelineUrlShortener.Data.Pagination;
using WizelineUrlShortener.DataAccess.Repositories.Interfaces;
using WizelineUrlShortener.Utilities;

namespace WizelineUrlShortener.Services
{
    public class UrlShortenerService : IUrlShortenerService
    {
        private readonly IUrlRepository _urlRepository;

        public UrlShortenerService(IUrlRepository urlRepository)
        {
            _urlRepository = urlRepository;
        }

        public async Task<IList<UrlShort>> ShortUrlAsync(AddShortUrlDto model)
        {
            var isModelHavingCustomKey = !string.IsNullOrEmpty(model.CustomKey);
            if (isModelHavingCustomKey)
            {
                if (await _urlRepository.IsShortUrlTaken(model.CustomKey))
                    return null;
            }

            if (!isModelHavingCustomKey)
            {
                var shortUrlVerify = await _urlRepository.GetListByUrl(model.Url);
                if (shortUrlVerify.Count != 0)
                {
                    return shortUrlVerify;
                }
            }

            var id = await _urlRepository.GetNextId();

            var shortUrlKey = isModelHavingCustomKey ? model.CustomKey : GenerateShortUrlKey(id);

            var entity = new UrlShort
            {
                ShortUrlKey = shortUrlKey,
                Url = model.Url
            };

            await _urlRepository.AddAsync(entity);

            _urlRepository.SaveChanges();

            return new List<UrlShort>(1) {entity};
        }

        public async Task<Uri> ShortToLongUrlAsync(string urlShortKey)
        {
            // Get url from db
            var urlShort = await _urlRepository.GetByShortUrlKey(urlShortKey);

            if (urlShort == null) return null;

            var uri = new Uri(urlShort.Url);

            return uri;
        }

        public async Task<IPage<ListShortUrlDto>> GetPage(string hostUrl, TablePageParamsDto pageParams)
        {
            var result = await _urlRepository.GetPageItems(pageParams);

            result = result.Select(x =>
            {
                x.ShortUrl = string.Concat(hostUrl, "/", x.ShortUrl);
                return x;
            }).ToList();

            var count = await _urlRepository.GetCount();

            return new Page<ListShortUrlDto>
            {
                Items = result,
                CurrentPage = pageParams.PageNumber,
                ItemsPerPage = pageParams.ItemsPerPage,
                TotalItems = count
            };
        }

        private static string GenerateShortUrlKey(ulong id)
        {
            var hash = string.Empty;
            var hashDigits = new LinkedList<ulong>();
            var dividend = id;

            while (dividend > 0)
            {
                var remainder = dividend % 62;
                dividend = dividend / 62;
                hashDigits.AddFirst(remainder);
            }

            return hashDigits.Aggregate(hash, (current, digit) => current + Constants.Base62Dictionary[digit]);
        }
    }
}