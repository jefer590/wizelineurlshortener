﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WizelineUrlShortener.Data.Dtos;
using WizelineUrlShortener.Data.Model;
using WizelineUrlShortener.Data.Pagination;

namespace WizelineUrlShortener.Services.Interfaces
{
    public interface IUrlShortenerService
    {
        Task<IList<UrlShort>> ShortUrlAsync(AddShortUrlDto model);

        Task<Uri> ShortToLongUrlAsync(string urlShortKey);

        Task<IPage<ListShortUrlDto>> GetPage(string hostUrl, TablePageParamsDto pageParams);
    }
}