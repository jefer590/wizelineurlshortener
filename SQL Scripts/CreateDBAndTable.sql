CREATE TABLE `urlshorts` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`url` VARCHAR(2048) NOT NULL,
	`shorturlkey` VARCHAR(2048) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `url` (`url`(255)),
	INDEX `shorturlkey` (`shorturlkey`(255))
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=6
;
