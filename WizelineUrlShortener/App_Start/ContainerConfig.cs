﻿using System.Reflection;

using Autofac;
using Autofac.Integration.WebApi;

using Owin;

using WizelineUrlShortener.DataAccess;
using WizelineUrlShortener.Services;

using WebApiGlobalConfiguration = System.Web.Http.GlobalConfiguration;

namespace WizelineUrlShortener.App_Start
{
	public static class ContainerConfig
	{
		public static void ConfigureAutofac(this IAppBuilder app)
		{
			var builder = new ContainerBuilder();
			var assembly = Assembly.GetExecutingAssembly();

			// Register Web API controllers.
			builder.RegisterApiControllers(assembly);

			RegisterDependencies(builder);

			// Build Dependencies
			var container = builder.Build();
			app.UseAutofacMiddleware(container);

			// Configure WebApi
			var config = WebApiGlobalConfiguration.Configuration;
			app.UseAutofacWebApi(config);
			var apiResolver = new AutofacWebApiDependencyResolver(container);
			config.DependencyResolver = apiResolver;
		}

		private static void RegisterDependencies(ContainerBuilder builder)
		{
			// Register Dependency Modules
			builder.RegisterModule(new WebModule());
			builder.RegisterModule(new DataAccessModule());
			builder.RegisterModule(new ServicesModule());
		}
	}
}