﻿using System.Web.Http;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WizelineUrlShortener
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Remove XML Formatter
            var formatters = GlobalConfiguration.Configuration.Formatters;
            formatters.Remove(formatters.XmlFormatter);

            // Use Newtonsoft serializer as the JSON Serializer in the webapi
            var jsonSerializer = formatters.JsonFormatter;
            jsonSerializer.SerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.None,
                TypeNameHandling = TypeNameHandling.Auto,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            // Web API routes
            config.MapHttpAttributeRoutes();
        }
    }
}