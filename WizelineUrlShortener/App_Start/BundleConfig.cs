﻿using System.Web.Optimization;

namespace WizelineUrlShortener
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Styles

            bundles.Add(new StyleBundle("~/Content/css/vendor")
                .Include("~/Content/styles/vendor/bootstrap/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Content/styles/vendor/font-awesome/font-awesome.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/css/app").Include(
                "~/Content/styles/app/bootstrap-paper/bootstrap.css", new CssRewriteUrlTransform()
            ).Include("~/Content/styles/app/site.css"));

            #endregion

            #region Scripts

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/vendor/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/vendor/bootstrap/bootstrap.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/vendor/angular/angular.js",
                "~/Scripts/vendor/angular/angular-route/angular-route.js",
                "~/Scripts/vendor/angular/angular-animate/angular-animate.js",
                "~/Scripts/vendor/angular/angular-touch/angular-touch.js",
                "~/Scripts/vendor/angular/angular-resource/angular-resource.js",
                "~/Scripts/vendor/angular/ui-bootstrap/ui-bootstrap.js",
                "~/Scripts/vendor/angular/angular-truncate/angular-truncate.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/app/app.js",
                "~/Scripts/app/app-routes.js",
                "~/Scripts/app/services/*.js",
                "~/Scripts/app/controllers/*.js"
            ));

            #endregion
        }
    }
}