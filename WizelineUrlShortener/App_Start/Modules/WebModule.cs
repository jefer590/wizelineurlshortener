﻿using Autofac;

using Nito.AsyncEx;

namespace WizelineUrlShortener
{
	public class WebModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.Register(x => new AsyncLock()).AsSelf();
		}
	}
}