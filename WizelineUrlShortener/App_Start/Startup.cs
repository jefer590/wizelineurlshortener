﻿using Microsoft.Owin;

using Owin;

using WizelineUrlShortener.App_Start;

[assembly: OwinStartup(typeof(WizelineUrlShortener.Startup))]

namespace WizelineUrlShortener
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.ConfigureAutofac();
        }
    }
}