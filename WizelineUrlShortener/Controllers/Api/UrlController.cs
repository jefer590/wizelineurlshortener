﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

using Nito.AsyncEx;

using WizelineUrlShortener.Data.Dtos;
using WizelineUrlShortener.Data.Model;
using WizelineUrlShortener.Services.Interfaces;
using WizelineUrlShortener.Web;

namespace WizelineUrlShortener.Controllers.Api
{
	[Route("api/url")]
	public class UrlController : ApiController
	{
		private readonly IUrlShortenerService _urlShortenerService;

		private readonly AsyncLock _mutex;

		public UrlController(IUrlShortenerService urlShortenerService, AsyncLock mutex)
		{
			_urlShortenerService = urlShortenerService;
			_mutex = mutex;
		}

		[HttpGet, Route("{shortKeyUrl}")]
		public async Task<IHttpActionResult> Get(string shortKeyUrl)
		{
			if (string.IsNullOrEmpty(shortKeyUrl)) return BadRequest("Empty Short Url Key");

			var resultUri = await _urlShortenerService.ShortToLongUrlAsync(shortKeyUrl);

			if (resultUri == null) return NotFound();

			return Redirect(resultUri);
		}

		[HttpPost, ModelValidation]
		public async Task<IHttpActionResult> CreatShortUrl([FromBody] AddShortUrlDto model)
		{
			IList<UrlShort> result;
			using (await _mutex.LockAsync())
			{
				result = await _urlShortenerService.ShortUrlAsync(model);
			}

			if (result == null) return Conflict();

		    if (result.Count == 1)
		    {
		        return Created($"{Request.RequestUri.GetLeftPart(UriPartial.Authority)}/{result[0].ShortUrlKey}", new {Result = result});
		    }
		    else
		    {
		        return Ok(new { Result = result });
		    }
		}

		[HttpGet]
		public async Task<IHttpActionResult> GetPage([FromUri] TablePageParamsDto pageParams)
		{
			var urlHost = Request.RequestUri.GetLeftPart(UriPartial.Authority);
			var result = await _urlShortenerService.GetPage(urlHost, pageParams);
			return Ok(result);
		}
	}
}