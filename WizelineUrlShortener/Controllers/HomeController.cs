﻿using System.Web.Mvc;

namespace WizelineUrlShortener.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UrlList()
        {
            return View();
        }
    }
}