(function() {
    "use strict";

    var app = angular.module(appName);

    var configFunction = function($stateProvider, $locationProvider, $urlRouterProvider) {
        $locationProvider.hashPrefix("!").html5Mode(false);
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state({
                name: "shortify",
                url: "/",
                templateUrl: "App/Shortify",
                controller: "UrlShortenerController"
            })
            .state({
                name: "list",
                url: "/list",
                templateUrl: "App/List",
                controller: "ListUrlsController"
            });
    }

    configFunction.$inject = ["$stateProvider", "$locationProvider", "$urlRouterProvider"];

    app.config(configFunction);
})();