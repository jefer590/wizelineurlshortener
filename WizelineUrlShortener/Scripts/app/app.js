(function() {
    // Initialize Angular App
    angular.module(appName, ["ui.router", "ngAnimate", "ngTouch", "ngResource", "ui.bootstrap", "truncate"]);
})();