(function() {
    "use strict";

    var app = angular.module(appName);

    app.controller("UrlShortenerController", UrlShortenerController);

    UrlShortenerController.$inject = ["$scope", "$location", "$window", "urlService"];

    function UrlShortenerController($scope, $location, $window, urlService) {
        $scope.model = { url: "", customKey: "" };
        $scope.maxUrlCharacters = 2048;
        $scope
            .urlRegex =
            "^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$";
        $scope.shortUrl = "";
        $scope.busy = false;
        $scope.useCustomKey = false;
        $scope.error = false;
        $scope.showList = false;
        $scope.listOfUrls = [];

        $scope.showUrlShortified = false;

        $scope.shortifyUrl = function(form) {
            if (!form || form.$invalid) return;
            $scope.busy = true;

            var model = angular.copy($scope.model);

            urlService.shortifyUrl(model).$promise
                .then(function(response) {
                    $scope.shortifyUrlForm.$setPristine();
                    $scope.model.url = "";
                    $scope.model.customKey = "";
                    var baseUrl = new $window.URL($location.absUrl()).origin;

                    if (response.result.length === 1) {
                        $scope.showUrlShortified = true;
                        $scope.shortUrl = baseUrl + "/" + response.result[0].shortUrlKey;
                    } else {
                        $scope.listOfUrls = angular.copy(response.result);
                        angular.forEach($scope.listOfUrls,
                            function(value) {
                                value.shortUrlKey = baseUrl + "/" + value.shortUrlKey;
                            });
                        $scope.showList = true;
                    }

                })
                .catch(function(error) {
                    $scope.error = true;
                    console.error(error);
                })
                .finally(function() {
                    $scope.busy = false;
                });
        };

        $scope.hasError = function(input) {
            return input.$invalid && !input.$pristine && input.$touched;
        }

        $scope.closeUrlAlert = function() {
            $scope.showUrlShortified = false;
        }

        $scope.closeErrorAlert = function() {
            $scope.closeErrorAlert = false;
        };

        $scope.closeListAlert = function () {
            $scope.showList = false;
        };
    }
})();