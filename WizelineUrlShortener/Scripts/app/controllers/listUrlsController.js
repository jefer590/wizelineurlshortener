(function() {
    "use strict";

    var app = angular.module(appName);

    app.controller("ListUrlsController", ListUrlsController);

    ListUrlsController.$inject = ["$scope", "urlService"];

    function ListUrlsController($scope, urlService) {
        $scope.items = [];
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;

        getPage();

        $scope.loadPage = function() {
            getPage();
        };

        $scope.areItemsEmpty = function() {
             return $scope.items.length === 0;
        };

        function getPage() {
            var params = {
                itemsPerPage: $scope.itemsPerPage,
                pageNumber: $scope.currentPage
            };

            urlService.getPage(params).$promise
                .then(function(response) {
                    $scope.items = response.items;
                    $scope.totalItems = response.totalItems;
                    $scope.currentPage = response.currentPage;
                    $scope.itemsPerPage = response.itemsPerPage;
                 })
                .catch(function(error) {
                    console.log(error);
                });
        }
    }
})();