(function() {
    var app = angular.module(appName);

    app.service("urlService", UrlService);

    UrlService.$inject = ["$resource"];

    function UrlService($resource) {
        var resourceUrl = $resource("/api/url",
                {},
                {}),
            self = this;

        self.getPage = function(params) {
            return resourceUrl.get(params);
        };

        self.shortifyUrl = function(model) {
            return resourceUrl.save({}, model);
        }
    }
})();