﻿(function() {
	"use strict";

	var app = angular.module(appName);
	app.factory("pendingHttpInterceptor", pendingHttpInterceptor);

	pendingHttpInterceptor.$inject = ["$q", "$rootScope"];

	function pendingHttpInterceptor($q, $rootScope) {
		var pending = 0;

		function updateInProgress() {
			$rootScope.ajaxInProgress = --pending > 0;
		}

		return {
			request: function(config) {
				pending++;
				$rootScope.ajaxInProgress = true;
				return config || $q.when(config);
			},
			requestError: function(response) {
				updateInProgress();
				return $q.reject(response);
			},
			response: function(response) {
				updateInProgress();
				return response || $q.when(response);
			},
			responseError: function(response) {
				updateInProgress();
				return $q.reject(response);
			}
		};
	}

	app.config([
		"$httpProvider", function($httpProvider) {
			$httpProvider.interceptors.push("pendingHttpInterceptor");
		}
	]);
})();