﻿namespace WizelineUrlShortener.Data.Model
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}