﻿namespace WizelineUrlShortener.Data.Model
{
    public class Entity : IEntity<long>
    {
        public long Id { get; set; }
    }
}