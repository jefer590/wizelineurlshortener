﻿namespace WizelineUrlShortener.Data.Model
{
    public class UrlShort : IEntity<ulong>
    {
        public ulong Id { get; set; }

        public string Url { get; set; }

        public string ShortUrlKey { get; set; }
    }
}