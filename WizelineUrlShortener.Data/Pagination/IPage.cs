﻿using System.Collections.Generic;

namespace WizelineUrlShortener.Data.Pagination
{
    public interface IPage<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Items { get; set; }

        int CurrentPage { get; set; }

        int ItemsPerPage { get; set; }

        long TotalItems { get; set; }
    }
}