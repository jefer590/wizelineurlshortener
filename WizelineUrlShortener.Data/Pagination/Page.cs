﻿using System.Collections.Generic;

namespace WizelineUrlShortener.Data.Pagination
{
    public class Page<TEntity> : IPage<TEntity> where TEntity : class
    {
        public IEnumerable<TEntity> Items { get; set; }
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }
        public long TotalItems { get; set; }
    }
}