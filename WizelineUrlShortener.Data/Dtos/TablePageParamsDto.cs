﻿namespace WizelineUrlShortener.Data.Dtos
{
    public class TablePageParamsDto
    {
        public int ItemsPerPage { get; set; }

        public int PageNumber { get; set; }
    }
}