﻿using System.ComponentModel.DataAnnotations;

namespace WizelineUrlShortener.Data.Dtos
{
    public class AddShortUrlDto
    {
        [Required]
        public string Url { get; set; }

        public string CustomKey { get; set; }
    }
}