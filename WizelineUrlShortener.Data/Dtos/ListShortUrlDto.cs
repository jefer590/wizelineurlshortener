﻿namespace WizelineUrlShortener.Data.Dtos
{
    public class ListShortUrlDto
    {
        public string ShortUrl { get; set; }

        public string LongUrl { get; set; }
    }
}